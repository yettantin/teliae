/** Chargement des EDI en erreur
   *
   * EDIEnErreur
   *
   * @param  mixed $idClientTrsp
   *  Identifiant du client transporteur
   * @param  mixed $iNbJours
   *  Nombres de jours correspondant à la période d'extraction
   *
   * @return bool|array
   */
  function EDIEnErreur($idClientTrsp, $iNbJours) {
    $Req = "SELECT" . " clienttrsp.Nom, compte.Nom, bordereau.NumBordereau, bordereau.idBordereau, bordereau.dtCreation, bordereau.iStatut, COUNT(expedent.idExpedent)  FROM bordereau";
    $Req .= " INNER JOIN compte ON bordereau.idCompte = compte.idCompte";
    $Req .= " INNER JOIN clienttrsp ON compte.idClientTrsp = clienttrsp.idClientTrsp";
    $Req .= " INNER JOIN expedent ON expedent.idBordereau = bordereau.idBordereau";
    $Req .= " WHERE
                (
                  (bordereau.iStatut IN (3,4,5,8) )
                  OR (bordereau.iStatut = 6 AND (bordereau.dtCreation + 600) <= UNIX_TIMESTAMP(NOW()))
                )
                AND clienttrsp.idClientTrsp = " . (int) $idClientTrsp. "
                AND (bordereau.dtCreation >= (UNIX_TIMESTAMP(NOW()) - (86400 * " . $iNbJours . ")))";
    $Req .= " GROUP BY clienttrsp.Nom, compte.Nom, bordereau.NumBordereau, bordereau.idBordereau, bordereau.dtCreation, bordereau.iStatut;";

  return executeQueryAndGetTable($Req);
  }
}