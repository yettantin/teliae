<?php

/**
 * @encoding     ISO-8859-1
 * @author       JULIETTE
 * @copyright    TELIAE 2019
 * @name         CTraitementAuto_File-EnvoiErreur.php
 * ===========================================================================
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Revision: $
 * $Id: $
 * ===========================================================================
 */

 class CTraitementAuto_EnvoiEnErreur extends CTraitementAuto_SpecAbs {

  private $sEmails;
  private $iDelaiMin;
  private $iNbEnvois;

  const CLE_EMAILS        = 'sEmails';
  const CLE_DELAI_MIN     = 'iDelaiMin';
  const CLE_NB_ENVOI      = 'iNbEnvoi';

  // getters et setters
  //=====================================================================================================
  function get_sEmails() {
    return $this->sEmails;
  }
  function get_iDelaiMin() {
    return $this->iDelaiMin;
  }
  function get_iNbEnvois() {
    return $this->iNbEnvois;
  }
  function set_sEmails($sEmails) {
    $this->sEmails = $sEmails;
  }
  function set_iDelaiMin($iDelaiMin) {
    $this->iDelaiMin = $iDelaiMin;
  }
  function set_iNbEnvois($iNbEnvois) {
    $this->iNbEnvois = $iNbEnvois;
  }

  /** Excécute le traitement automatique : Remonte les Envois en erreur et envoie un Email à l'adresse renseignée dans le formaulaire
   * @return void
   */
  public function executerTraitement() {  
    
    // Si le traitement automatique concerne tous les clientTrsp de la station
        $this->envoiMail(); 
  }
  
  /** Envoie les Emails avec leur pièce jointe
   * @return Bool
   */
  public function  envoiMail() { 

    $idClientTrsp = $this->idClientTrsp; 
    $iNbEnvois = $this->iNbEnvois;
    $iDelaiMin = $this->iDelaiMin;
    $iDelai = $this->convertirDelai($iDelaiMin); 

    $oFileEnvoi = new CFileEnvoi;
    $tabDetailEnvoisErreur = $oFileEnvoi->recupererTabNbEnvoiEchecParClient($idClientTrsp, $iDelai); 

    // Calcul le nombre total d'envois en erreur
    $iTotalEnvoiErreur = 0;
    foreach($tabDetailEnvoisErreur as $index=>$ligne) { 
      $iTotalEnvoiErreur = $iTotalEnvoiErreur + $tabDetailEnvoisErreur[$index][1]; 
    }

    // Contôle l'existance d'envois en erreur
    if($iTotalEnvoiErreur < $iNbEnvois) {
      return false;
    }
    // Prépare l'email   
    $oMail = new utils_email();
    $oMail->set_sMessage($this->emailEnvoisEnErreur());
    $oMail->set_sSujet("Envois en erreur du " . date('d/m/Y'));
    $oMail->set_tabDestinataire(explode(";", $this->get_sEmails()));
    //$oMail->set_tabSCopieConforme(explode(";", CConfigStation::$sAdrEmailNotificationEDIErreur)); 
    $oMail->set_iTypeMail(utils_email::IENVOI_PAR_NOTIFIACTION);

    $oMail->envoie_email_html(false);  
    return true;
  }


  /** Contenu du mail format html    
   * emailEDIErreurHtml   
   * @return string : $sContenuMail
   */
  public function emailEnvoisEnErreur() {
    $idClientTrsp = $this->idClientTrsp;
    $iDelaiMin = $this->iDelaiMin;
    $iDelai = $this->convertirDelai($iDelaiMin);

    $oFileEnvoi = new CFileEnvoi;
    $tabDetailEnvoisErreur = $oFileEnvoi->detailEnvoiEchec($idClientTrsp, $iDelai);

    $sContenuMail = '<html><head><title>station-chargeur.com</title>' . "\n"
                  . '</head><body>' . "\n"
                  . '<p>Veuillez trouvez ci-dessous le rapport des envois en erreurs</p>'
                  . $tabDetailEnvoisErreur
                  . '</body></html>';
    return $sContenuMail;
  }

  /**Initialise les paramètres si renseignés au préalable
   * @return void
   */
  public function initialiserParametre() {
    $tab = $this->deduireTabDonnees();
    if (isset($tab[self::CLE_NB_ENVOI])) {
      $this->iNbEnvois = $tab[self::CLE_NB_ENVOI];
    }
    if (isset($tab[self::CLE_DELAI_MIN])) {
      $this->iDelaiMin = $tab[self::CLE_DELAI_MIN];
    }
    if (isset($tab[self::CLE_EMAILS])) {
      $this->sEmails = $tab[self::CLE_EMAILS];
    }
  }

  /** Vérifie l'intégrité des données spécifiées par l'utilisateur
   * @return bool
   */
  public function controlerSpecifique() {
    if ($this->get_iNbEnvois() == '') {
      $this->ajouterErreur('Il est obligatoire de spécifier le nombre minimal d\'envois en erreur.');
      return false;
    }
    if (is_numeric($this->get_iNbEnvois()) === false) {
      $this->ajouterErreur('Le nombre d\'envois doit être un nombre entier.'); 
      return false;
    }
    if ($this->get_iDelaiMin() == '') {
      $this->ajouterErreur('Il est obligatoire de spécifier le délai de non envoi.');
      return false;
    }
    if (is_numeric($this->get_iDelaiMin()) === false) {
      $this->ajouterErreur('Le délai doit être un nombre entier.'); 
      return false;
    }
    if ($this->get_sEmails() == '') {
      $this->ajouterErreur('Il est obligatoire de spécifier un email de destinataire.');
      return false;
    }
    if (is_string($this->get_sEmails()) === false) {
      $this->ajouterErreur('L\'email doit être un email.'); 
      return false;
    }
    return true;
  }

  /** Prérempli le formulaire et initialise le type d'opération
   * @return void
   */
  public function intitialiser() {
  // iOperation
      $this->set_iOperation(CTraitementAuto::IOPERATION_FILEENVOI_ERREUR);

  // bActif
      $this->set_bActif(true);

  // Minutes
      $this->set_sMinutes('00');

  // Heures
      $this->set_sHeures('05');

  // Description
      $this->set_sDescription('Envoi en erreur');

  // On  n'affiche pas les paramètres supplémentaires
      $this->set_m_bAfficher_parametres(true);
  }

  /**
   * Formulaire des paramètres du traitement automatique
   * @param  mixed $oFormulaire
   * @return void
   */
  public function affichageParametre(CFormulaire $oFormulaire) {
    $this->initialiserParametre();

    $oFormulaire->AddNEdit('Nombre de mails non envoyés', 'CTraitementAuto_EnvoiEnErreur_NbEnvois', $this->iNbEnvois);
    $oFormulaire->AddNEdit('Délai de non envoi (en minutes)', 'CTraitementAuto_EnvoiEnErreur_DelaiEnvoi', $this->iDelaiMin);
    $oFormulaire->AddEdit('Email destinataire', 'CTraitementAuto_EnvoiEnErreur_EmailDestinataire', $this->sEmails);
  }
  
  /** Converti le délai en minute renseigné dans le formulaire en seconde
   * @param  int $iDelaiMin
   * @return integer : $iDelai
   */
  public function convertirDelai($iDelaiMin) {
    $iDelaiMin = $this->iDelaiMin;
    $iDelai = $iDelaiMin * 60;
    return $iDelai;
  }

  /**
   * donnerTabDonnee
   * @return array
   */
  public function donnerTabDonnee() {
    return array();
  }

  /** Récupère les données spécifiées par l'utilisateur
   * @return bool
   */
  public function recupererParametre() {
    $this->set_iNbEnvois(recupererPost('CTraitementAuto_EnvoiEnErreur_NbEnvois'));
    $this->set_iDelaiMin(recupererPost('CTraitementAuto_EnvoiEnErreur_DelaiEnvoi'));
    $this->set_sEmails(recupererPost('CTraitementAuto_EnvoiEnErreur_EmailDestinataire'));

    $this->set_sParametres(
      self::CLE_NB_ENVOI . ':' . $this->get_iNbEnvois() . "\n"
     . self::CLE_DELAI_MIN . ':' . $this->get_iDelaiMin() . "\n"
     . self::CLE_EMAILS . ':' . $this->get_sEmails()
    );
    return false;
  }
}
