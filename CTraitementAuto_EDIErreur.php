<?php

/**
 * @encoding     ISO-8859-1
 * @author       JULIETTE
 * @copyright    TELIAE 2019
 * @name         CTraitementAuto_EDIErreur.php
 * ===========================================================================
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Revision: $
 * $Id: $
 * ===========================================================================
 */

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\Style\Alignment;
  use PhpOffice\PhpSpreadsheet\Style\Fill;

 class CTraitementAuto_EDIErreur extends CTraitementAuto_SpecAbs {

  private $sEmails;
  private $tabBordereau;
  private $iNbJours;
  private $bFormatPJ;
  // Tableau de correspondance des statuts d'erreur
  public $tabTypeErreurEDI = array (
    '3' => 'Erreur d écriture',
    '4' => 'Erreur Email',
    '5' => 'Erreur FTP',
    '6' => 'Erreur de génération',
    '8' => 'Erreur de génération',
    ''  => 'PAS DE STATUT ERREUR'
  );

  const CLE_EMAILS        = 'sEmails';
  const CLE_NBJOURS       = 'iNbJours';
  const CLE_FORMATPJ      = 'bFormatPJExcel';

  // getters et setters
  //=====================================================================================================
  function get_sEmails() {
    return $this->sEmails;
  }

  function get_iNbJours() {
    return $this->iNbJours;
  }

  function get_bFormatPJ() {
    return $this->bFormatPJ;
  }

  function set_sEmails($sEmails) {
    $this->sEmails = $sEmails;
  }

  function set_iNbJours($iNbJours) {
    $this->iNbJours = $iNbJours;
  }

  function set_bFormatPJ($bFormatPJExcel) {
    $this->bFormatPJ = $bFormatPJExcel;
  }

  /** Excécute le traitement automatique : Remonte les EDI en erreur et envoie l'email au client concerné
   * 
   * executerTraitement
   *
   * @return void
   */
  public function executerTraitement() {  
    
    // Charge l'id du client transporteur. ('idClientTrsp' => CTraitementAuto)
    $idClientTrsp = $this->idClientTrsp; 

    // Si le traitement automatique concerne tous les clientTrsp de la station
    if($idClientTrsp == 0) {  
      //Récupération des Clients transporteur (objet)
      $oTabClient = CClientTrsp::RecupererOClientTrspAvecMailAnomalie();

      // Envoie les emails à chaque clientTrsp   
      foreach($oTabClient as $oClient) {
        $this->set_sEmails($oClient->sEmailAlertePourAnnomalie);        
        $this->set_idClientTrsp($oClient->get_idClientTrsp());
        $this->envoiMail(); 
      }
    }

    // Si le traitement concerne un clientTrsp précis
    else {
      $oClientTrsp = new CClientTrsp();
      if ($oClientTrsp->Chargement($idClientTrsp)
          && $oClientTrsp->sEmailAlertePourAnnomalie <> ''){
        //envoi du mail
        $this->set_sEmails($oClientTrsp->sEmailAlertePourAnnomalie);     
        $this->envoiMail();
      }           
    }
  }

  /** Récupère la liste des numéros de bordereau sur une période donnée
   * 
   * recupererNumBordereau
   *
   * @param  mixed $idClientTrsp
   *
   * @return void
   */  
  public function recupererNumBordereau($idClientTrsp) {
    // Charge le nombre de jours de la période sur laquelle on veut remonter les EDI en erreur
    $iNbJours = $this->iNbJours;

    // Récupère des données du bordereaux et du clientTrsp
    $oBordereau = new CBordereau();
    $this->tabBordereau = $oBordereau->EDIEnErreur($idClientTrsp, $iNbJours);  
    $tabBordereau = $this->tabBordereau; 
    if($tabBordereau[0][5] == '') { 
      $this->tabBordereau = false;
    }  
  }
  
  /** Envoie les Emails avec leur pièce jointe
   * 
   * envoiMail
   *
   * @return void
   */
  public function  envoiMail() { 

    $idClientTrsp = $this->idClientTrsp; 
    $this->recupererNumBordereau($idClientTrsp); 
    $bFormatPJExcel = $this->bFormatPJ;
    $sExtension = '';
    $sContenu = '';
    $iNbJours = $this->iNbJours;
    $DateDebutExtraction = time() - (24 * 60 * 60 * $iNbJours);

    // Contôle l'existance d'EDI en erreur
    if ($this->tabBordereau == false) { 
      return false;
    }
    else {
      // Prépare l'email   
      $oMail = new utils_email();
      $oMail->set_sMessage($this->emailEDIErreurHtml());
      $oMail->set_sSujet("EDI en erreur du " . date('d/m/Y', $DateDebutExtraction) . " au " . date('d/m/Y'));
      $oMail->set_tabDestinataire(explode(";", $this->get_sEmails()));
      $oMail->set_tabSCopieConforme(explode(";", CConfigStation::$sAdrEmailNotificationEDIErreur)); 
      $oMail->set_iTypeMail(utils_email::IENVOI_PAR_NOTIFIACTION);

      // Prépare la pièce jointe au format xlsx
      if($bFormatPJExcel == true) {
        $sContenu = $this->creerContenuPJxlsx(); 
        $sExtension = 'xlsx';
      }
      // Prépare la pièce jointe au format csv (format par défaut)
      else {
        $sContenu = $this->creerContenuPJcsv(); 
        $sExtension = 'csv';
      } 

      $oMail->ajouterFichier('EDIEnErreur.' . $sExtension, $sContenu);     
      $oMail->envoie_email_html(false);  
    }
  }

  /** Encode en UTF-8 le flu sortant pour fichier excel 
   *  + Applique un alignement horizontal dasn la cellule 
   * 
   * setCell
   *
   * @param  mixed $p_feuilleCalcul
   *  Feuille de calcul active
   * @param  mixed $p_cellule
   *  Coordonnées de la cellule
   * @param  mixed $p_value
   *  Valeur attribuée à la cellule
   * @param  bool $p_alignement
   *  Alignement du contenu : horizontal_center
   *
   * @return $cell
   */
  private function setCell($p_feuilleCalcul, $p_cellule, $p_value, $p_alignement) {

    $cell = $p_feuilleCalcul->getCell($p_cellule); 
    $p_feuilleCalcul->setCellValue($p_cellule, utils_encodage::convertStringTeliaeSortant($p_value, "UTF-8"));  
    
    if($p_alignement){
      $cell->getStyle($cell)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
    }
      return $cell;
  }

  /** Crée la liste des EDI en erreur de la pièce jointe en .xlsx
   * 
   * creerContenuPJxlsx
   *
   * @return void
   */
  public function creerContenuPJxlsx() {

    $iNbJours = $this->iNbJours;
    $DateDebutExtraction = time() - (24 * 60 * 60 * $iNbJours);

    // Préparation du fichier .xlsx
    $spreadsheet = new Spreadsheet();
    $feuilleCalcul = $spreadsheet->getActiveSheet();

    // Réglage de la police par défaut
    $spreadsheet->getDefaultStyle()
      ->getFont()
      ->setName('Calibri')
      ->setSize(12);

    // Mise en page de l'entête
    // Tableau de style de l'entête
    $tabStyleEntete = [
      'font' => [
        'color' => [
          'rgb' => 'FFFFFF' 
        ],
        'bold' => true,
        'size' => 20
      ],
      'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => [
          'rgb' => '538ED5'
        ]
      ],
    ]; 

    // En tête du document
    $this ->setCell($feuilleCalcul,'A1',"EDI en erreur du " . date('d/m/Y', $DateDebutExtraction) . " au " . date('d/m/Y'), true);
    $feuilleCalcul ->mergeCells('A1:E1');
    $feuilleCalcul ->getStyle('A1:E1')->applyFromArray($tabStyleEntete);
    
    // Mise en page du tableau   
    $tabStyleEnteteTableau = [
      'font' => [
        'color' => [
          'rgb' => 'FFFFFF' 
        ],
        'bold' => true,
        'size' => 14
      ],
      'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => [
          'rgb' => '538ED5'
        ]
      ],
    ]; 
    
    // Déclare la référence des colonnes
    $Colonne = array (
      'ColNomCompte'     => 'A',    
      'ColNumBordereau'  => 'B',
      'ColCreation'      => 'C',
      'ColStatutErreur'  => 'E',
      'ColQtiteExpe'     => 'D'
    );

    // Déclare la référence de la ligne d'entête du tableau
    $iPremiereLigneTab = 3;

    // Mise en page tableau
    foreach($Colonne as $cle => $ligne) {       
      $feuilleCalcul->getStyle($Colonne[$cle] . $iPremiereLigneTab)->applyFromArray($tabStyleEnteteTableau);
      $feuilleCalcul->getColumnDimension($Colonne[$cle])->setWidth(38);  
    }

    // En tête du tableau
    $this->setCell($feuilleCalcul,$Colonne['ColNomCompte'] . $iPremiereLigneTab, 'Compte', true);
    $this->setCell($feuilleCalcul,$Colonne['ColNumBordereau'] . $iPremiereLigneTab, 'Numéro de bordereau', true);
    $this->setCell($feuilleCalcul,$Colonne['ColCreation'] . $iPremiereLigneTab, 'Date de création', true);
    $this->setCell($feuilleCalcul,$Colonne['ColQtiteExpe'] . $iPremiereLigneTab, 'Quantité d\' expedition', true);
    $this->setCell($feuilleCalcul,$Colonne['ColStatutErreur'] . $iPremiereLigneTab, 'Statut d\' erreur', true);

    // Récupère le tableau renvoyé par la fonction 'recupererNumBordereau($idClientTrsp)'
    $tabBordereau = $this->tabBordereau; 

    // Variable utilisée pour nommé le fichier dans tmp
    $sNomClient = '';

    foreach($tabBordereau as $index => $ligne) {

      $this->setCell($feuilleCalcul,$Colonne['ColNomCompte'] . ($iPremiereLigneTab + $index + 1), $ligne[1], true);
      $this->setCell($feuilleCalcul,$Colonne['ColNumBordereau'] . ($iPremiereLigneTab + $index + 1) , $ligne[2], true);
      $this->setCell($feuilleCalcul,$Colonne['ColCreation'] . ($iPremiereLigneTab + $index + 1) , date('d-m-Y', $ligne[4]), true);
      $this->setCell($feuilleCalcul,$Colonne['ColStatutErreur'] . ($iPremiereLigneTab + $index + 1) , $this->tabTypeErreurEDI[$ligne[5]], true);
      $this->setCell($feuilleCalcul,$Colonne['ColQtiteExpe'] . ($iPremiereLigneTab + $index + 1) , $ligne[6], true);

      if ($index == 0) {
        $sNomClient = $ligne[0]; 
      }
    }
      
    // écriture et édition du fichier .xlsx
    $oWriterXlsx = new Xlsx($spreadsheet);
    $oWriterXlsx->save('../tmp/'. $sNomClient . '.xlsx');
    $sfileContent = file_get_contents('../tmp/'. $sNomClient . '.xlsx');

    return $sfileContent;
  }

  /** Crée la liste des EDI en erreur de la pièce jointe en .csv
   * 
   * creerContenuPJcsv
   *
   * @return $fileContent
   */
  public function creerContenuPJcsv() {
    $iNbJours = $this->iNbJours;
    $DateDebutExtraction = time() - (24 * 60 * 60 * $iNbJours);

    $sfileContent = "EDI en erreur du " . date('d/m/Y', $DateDebutExtraction) . " au " . date('d/m/Y') . chr(13).chr(10);
    $sfileContent .= "Nom du compte;";
    $sfileContent .= "Numéro de bordereau;";
    $sfileContent .= "Date de création;";
    $sfileContent .= "Quantité d'expéditions;";
    $sfileContent .= "Statut;" . chr(13).chr(10);

    foreach($this->tabBordereau as $ligne) {
      $sfileContent = $sfileContent . $ligne[1] . ";";
      $sfileContent .=  $ligne[2] . ";";
      $sfileContent .=  date('d/m/Y', $ligne[4]) . ";";
      $sfileContent .=  $ligne[6] . ";";
      $sfileContent .=  $this->tabTypeErreurEDI[$ligne[5]] . ";" .  chr(13).chr(10);

    }
    return $sfileContent;
  }  

  /** Contenu du mail
   * 
   * emailEDIErreurHtml
   *
   * @return $sContenuMail
   */
  public function emailEDIErreurHtml() {
    $sContenuMail = '<html><head><title>station-chargeur.com</title>' . "\n"
                  . '</head><body>' . "\n"
                  . '<p>Veuillez trouver ci-joint le rapport d\'erreur EDI</p>'
                  . '</body></html>';
    return $sContenuMail;
  }

  /**
   * initialiserParametre
   *
   * @return void
   */
  public function initialiserParametre() {
    $tab = $this->deduireTabDonnees();
    // if (isset($tab[self::CLE_EMAILS])) {
    //   $this->sEmails = $tab[self::CLE_EMAILS];
    // }
    if (isset($tab[self::CLE_NBJOURS])) {
      $this->iNbJours = $tab[self::CLE_NBJOURS];
    }
    if (isset($tab[self::CLE_FORMATPJ])) {
      $this->bFormatPJ = $tab[self::CLE_FORMATPJ];
    }

  }

  /** Vérifie l'intégrité des données spécifiées par l'utilisateur
   * 
   * controlerSpecifique
   *
   * @return bool
   */
  public function controlerSpecifique() {
    if ($this->get_iNbJours() == '') {
      $this->ajouterErreur('Il est obligatoire de spécifier la période d\'extraction.');
      return false;
    }
    if (is_numeric($this->get_iNbJours()) === false) {
      $this->ajouterErreur('La période d\'extraction doit être un nombre entier.'); 
      return false;
    }

    return true;
  }

  /** Prérempli le formulaire et initialise le type d'opération
   * 
   * intitialiser
   *
   * @return void
   */
  public function intitialiser() {
  // iOperation
      $this->set_iOperation(CTraitementAuto::IOPERATION_EDI_ERREUR);

  // bActif
      $this->set_bActif(true);

  // Minutes
      $this->set_sMinutes('00');

  // Heures
      $this->set_sHeures('05');

  // Description
      $this->set_sDescription('EDI en erreur');

  // On  n'affiche pas les paramètres supplémentaires
      $this->set_m_bAfficher_parametres(true);
  }

  /**
   * affichageParametre
   *
   * @param  mixed $oFormulaire
   *
   * @return void
   */
  public function affichageParametre(CFormulaire $oFormulaire) {

    $this->initialiserParametre();

    $oFormulaire->AddNEdit('Période d\'extraction (en jours)', 'CTraitementAuto_EDIErreur_PeriodeExtraction', $this->iNbJours);
    $oFormulaire->AddCheckBox('Pièce jointe format Excel', 'CTraitementAuto_EDIErreur_PieceJointeXlsx', $this->bFormatPJ);
    $oFormulaire->AddLabel('', 'LabelFormatPJDefaut','(Format par défaut : CSV)');
  }

  /**
   * donnerTabDonnee
   *
   * @return array
   */
  public function donnerTabDonnee() {
    return array();
  }

  /** Récupère les données spécifiées par l'utilisateur
   * 
   * recupererParametre
   *
   * @return void||bool
   */
  public function recupererParametre() {
    $this->set_iNbJours(recupererPost("CTraitementAuto_EDIErreur_PeriodeExtraction"));
    $this->set_bFormatPJ(recupererPost('CTraitementAuto_EDIErreur_PieceJointeXlsx'));

    $this->set_sParametres(
      self::CLE_NBJOURS . ':' . $this->get_iNbJours() . "\n"
     . self::CLE_FORMATPJ . ':' . $this->get_bFormatPJ()
    );
    return false;
  }
}
