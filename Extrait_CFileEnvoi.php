//=================================================================================================
  /**
   * Récupère les envois en échec d'un clientTrsp suivant un délai de non transmission   
   * @param INT $idClientTrsp
   * @param INT $iDelai
   * @return boolean||array : $tabFileEnvoiEchec
   */
  public function recupererTabNbEnvoiEchecParClient($idClientTrsp, $iDelai) {
    $req  = 'SELECT ' . 'clienttrsp.Nom, COUNT(fileenvoi.idFileEnvoi) ';
    $req .= 'FROM fileenvoi ';
    $req .= 'JOIN clienttrsp ON clienttrsp.idClientTrsp = fileenvoi.idClientTrsp ';
    $req .= 'WHERE ';

    // conditionne la selection en fonction d'un clientTrsp spécifique (cas de l'interface Admin)
    if($idClientTrsp != 0) {
      $req .= 'fileenvoi.idClientTrsp = ' . $idClientTrsp . ' AND ';
    }    

    $req .= '(fileenvoi.iEtat in (0,2,3) ';
    $req .= 'AND ' . '((fileenvoi.iTimeStampEnvoi + ' . $iDelai . ') ' . ' <= UNIX_TIMESTAMP(NOW()))) ';
    $req .= 'GROUP BY fileenvoi.idClientTrsp;';

    $tabFileEnvoiEchec = executeQueryAndGetTable($req); 

    return $tabFileEnvoiEchec; 
  }
  
  /**
   * Affiche le(s) nom(s) du clientTrsp avec le total de ses(leurs) envois en erreurs
   * @param  mixed $idClientTrsp
   * @param  mixed $iDelai
   * @return string
   */
  public function detailEnvoiEchec($idClientTrsp, $iDelai) {     
    $tabFileEnvoiEchec = $this->recupererTabNbEnvoiEchecParClient($idClientTrsp, $iDelai);  //var_dump($tabFileEnvoiEchec);

    // Dans le cas où aucun envoi est en erreur
    if($tabFileEnvoiEchec == false) {
      $iTotalFileEnvoiEchecClient = 0;
      return ('<div class="cl_totEnvoiErreur">' .  $iTotalFileEnvoiEchecClient . '</div>');
    }

    $sAfficherTableau = '';

    foreach($tabFileEnvoiEchec as $index=>$ligne) { 
      $sNomClientTrsp = $tabFileEnvoiEchec[$index][0]; 
      $iTotalFileEnvoiEchecClient = $tabFileEnvoiEchec[$index][1];

      // Condition pour l'interface sadmin
      if($idClientTrsp == 0) {
        $sAfficherTableau .=  '<tr class="cl_TblListeDonne"><td class="cl_TblListeDonne">' . $sNomClientTrsp . '</td><td>' . $iTotalFileEnvoiEchecClient . '</td></td>';
      }

      // Interface admin
      else {
        return ('<div class="cl_totEnvoiErreur">' .  $iTotalFileEnvoiEchecClient . '</div>');
      }
    }
    return ('<TABLE class="cl_TblListeDonne">' . $sAfficherTableau . '</TABLE>');
  }


  /**
   * Calcul le nombre total d'envoi en échec sur tous clientsTrsp 
   * @param  mixed $idClientTrsp
   * @param  mixed $iDelai
   * @return integer
   */
  public function totalEnvoiEchec($idClientTrsp, $iDelai) {
    $tabFileEnvoiEchec = $this->recupererTabNbEnvoiEchecParClient($idClientTrsp, $iDelai); 
    $tabTotalFileEnvoi = array(); 

    if($tabFileEnvoiEchec === false) {
      $iTotalFileEnvoiEchec = 0;
      return $iTotalFileEnvoiEchec;
    }

    foreach($tabFileEnvoiEchec as $index=>$ligne) {
      $iNbEnvoiEchec = $tabFileEnvoiEchec[$index][1];
      array_push($tabTotalFileEnvoi, $iNbEnvoiEchec);
    }
     
    $iTotalFileEnvoiEchec = array_sum($tabTotalFileEnvoi);        
    return $iTotalFileEnvoiEchec;
  }